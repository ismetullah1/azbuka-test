﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent (typeof(Animator))]
[RequireComponent (typeof(CanvasGroup))]
public class UI_Screen : MonoBehaviour
{
	#region Variables
	[Header ("Main Properties")]
	public Selectable m_StartSelectable;

	[Header ("Screen Events")]
	public UnityEvent onScreenStart = new UnityEvent ();
	public UnityEvent onScreenClose = new UnityEvent ();

	public Animator animator;
	#endregion

	#region Main Methods
	// Use this for initialization
	void Start ()
	{
		//animator = GetComponent <Animator> ();
		if (m_StartSelectable) {
			EventSystem.current.SetSelectedGameObject (m_StartSelectable.gameObject);
		}
	}
	#endregion

	#region Helper Methods
	public void StartScreen ()
	{
		gameObject.SetActive (true);
		HandleAnimator ("show");
		if (onScreenStart != null) {
			onScreenStart.Invoke ();
		}
	}

	public void CloseScreen ()
	{
		gameObject.SetActive (false);
		HandleAnimator ("hide");
		if (onScreenClose != null) {
			onScreenClose.Invoke ();
		}
	}

	void HandleAnimator (string aTrigger)
	{
		if (animator) {
			animator.SetTrigger (aTrigger);
		}
	}
	#endregion
}
