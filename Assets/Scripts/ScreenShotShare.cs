﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.InteropServices;

public class ScreenShotShare : MonoBehaviour
{

	public string subject, ShareMessage, url;
	private bool isProcessing = false;
	public string ScreenshotName = "DenysScreenshot.png";

	public void ShareScreenshotWithText ()
	{
		// Share();

	}

	public void Share ()
	{
		#if UNITY_ANDROID
		if (!isProcessing)
			StartCoroutine (TakeScreenshot ());
		#elif UNITY_IOS
		if(!isProcessing)
		StartCoroutine( CallSocialShareRoutine() );
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}
	#if UNITY_ANDROID
	public IEnumerator ShareScreenshot ()
	{
		isProcessing = true;
		

		// wait for graphics to render
		yield return new WaitForEndOfFrame ();
		string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName;
		ScreenCapture.CaptureScreenshot (ScreenshotName);



//		yield return new WaitForSeconds (1f);
//		if (!Application.isEditor) {
//
//
//			AndroidJavaClass intentClass = new AndroidJavaClass ("android.content.Intent");
//			AndroidJavaObject intentObject = new AndroidJavaObject ("android.content.Intent");
//
//			intentObject.Call<AndroidJavaObject> ("setAction", intentClass.GetStatic<string> ("ACTION_SEND"));
//			AndroidJavaClass uriClass = new AndroidJavaClass ("android.net.Uri");
//			AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject> ("parse", "file://" + screenShotPath);
//			intentObject.Call<AndroidJavaObject> ("putExtra", intentClass.GetStatic<string> ("EXTRA_STREAM"), uriObject);
//			intentObject.Call<AndroidJavaObject> ("setType", "image/png");
//
//			intentObject.Call<AndroidJavaObject> ("putExtra", intentClass.GetStatic<string> ("EXTRA_TEXT"), ShareMessage);
//
//			AndroidJavaClass unity = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
//			AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject> ("currentActivity");
//
//			AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject> ("createChooser", intentObject, "Share Picture");
//			currentActivity.Call ("startActivity", jChooser);
//
//		}
		isProcessing = false;
	}

	private IEnumerator TakeScreenshot ()
	{
		yield return new WaitForEndOfFrame ();

		//INITIAL SETUP
		string myFilename = "Image.png";
		string myDefaultLocation = Application.persistentDataPath + "/" + myFilename;
		//EXAMPLE OF DIRECTLY ACCESSING THE Camera FOLDER OF THE GALLERY
		//string myFolderLocation = "/storage/emulated/0/DCIM/Camera/";
		//EXAMPLE OF BACKING INTO THE Camera FOLDER OF THE GALLERY
		//string myFolderLocation = Application.persistentDataPath + "/../../../../DCIM/Camera/";
		//EXAMPLE OF DIRECTLY ACCESSING A CUSTOM FOLDER OF THE GALLERY
		string myFolderLocation = "/storage/emulated/0/DCIM/Azbuka/";
		string myScreenshotLocation = myFolderLocation + myFilename;

		//ENSURE THAT FOLDER LOCATION EXISTS
		if (!System.IO.Directory.Exists (myFolderLocation)) {
			System.IO.Directory.CreateDirectory (myFolderLocation);
		}

		//TAKE THE SCREENSHOT AND AUTOMATICALLY SAVE IT TO THE DEFAULT LOCATION.
		ScreenCapture.CaptureScreenshot (myFilename);

		//MOVE THE SCREENSHOT WHERE WE WANT IT TO BE STORED
		System.IO.File.Move (myDefaultLocation, myFolderLocation);

		//REFRESHING THE ANDROID PHONE PHOTO GALLERY IS BEGUN
		AndroidJavaClass classPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject> ("currentActivity");        
		AndroidJavaClass classUri = new AndroidJavaClass ("android.net.Uri");        
		AndroidJavaObject objIntent = new AndroidJavaObject ("android.content.Intent", new object[2] {
			"android.intent.action.ACTION_MEDIA_MOUNTED",
			classUri.CallStatic<AndroidJavaObject> ("parse", "file://" + myScreenshotLocation)
		});        
		objActivity.Call ("sendBroadcast", objIntent);
		//REFRESHING THE ANDROID PHONE PHOTO GALLERY IS COMPLETE

		//AUTO LAUNCH/VIEW THE SCREENSHOT IN THE PHOTO GALLERY
		Application.OpenURL(myScreenshotLocation);
		//AFTERWARDS IF YOU MANUALLY GO TO YOUR PHOTO GALLERY, 
		//YOU WILL SEE THE FOLDER WE CREATED CALLED "myFolder"
	}

	#endif
	#if UNITY_IOS
		public struct ConfigStruct
		{
		public string title;
		public string message;
		}

		[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

		public struct SocialSharingStruct
		{
		public string text;
		public string url;
		public string image;
		public string subject;
		}

		[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

		public void CallSocialShare(string title, string message)
		{
		ConfigStruct conf = new ConfigStruct();
		conf.title = title;
		conf.message = message;
		showAlertMessage(ref conf);
		isProcessing = false;
		}

		public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
		{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
		}
		IEnumerator CallSocialShareRoutine()
		{
		isProcessing = true;
		string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName;
		Application.CaptureScreenshot(ScreenshotName);
		yield return new WaitForSeconds(1f);
		CallSocialShareAdvanced(ShareMessage, subject, url, screenShotPath);

		}

	#endif
}