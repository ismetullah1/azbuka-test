﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CameraController : MonoBehaviour {

	private bool cameramode = false;
	private bool isFlashOn = false;
	public void OnCameraChangeMode()
	{
		Vuforia.CameraDevice.CameraDirection currentDir = Vuforia.CameraDevice.Instance.GetCameraDirection();
		if (!cameramode)
		{
			RestartCamera(Vuforia.CameraDevice.CameraDirection.CAMERA_FRONT);
			Debug.Log("Back Camera");
		}
		else
		{
			RestartCamera(Vuforia.CameraDevice.CameraDirection.CAMERA_BACK);
			Debug.Log("Front Camera");
		}
		cameramode = !cameramode;
	}
		
	public void RestartCamera(){
		RestartCamera (Vuforia.CameraDevice.Instance.GetCameraDirection ());
	}

	public void OnFlashClicked(){
		isFlashOn = !isFlashOn;
		CameraDevice.Instance.SetFlashTorchMode (isFlashOn);
	}

	private void RestartCamera(Vuforia.CameraDevice.CameraDirection newDir)
	{
		Vuforia.CameraDevice.Instance.Stop();
		Vuforia.CameraDevice.Instance.Deinit();
		Vuforia.CameraDevice.Instance.Init(newDir);
		Vuforia.CameraDevice.Instance.Start();
	}

}
