﻿using UnityEngine;
using System.Collections;
using System.IO;

using Vuforia;

public class CameraImage : MonoBehaviour
{

	#region PRIVATE_MEMBERS

	private Image.PIXEL_FORMAT mPixelFormat = Image.PIXEL_FORMAT.UNKNOWN_FORMAT;

	private bool mAccessCameraImage = true;
	private bool mFormatRegistered = false;

	#endregion // PRIVATE_MEMBERS

	#region MONOBEHAVIOUR_METHODS

	void Start()
	{

		#if UNITY_EDITOR
		mPixelFormat = Image.PIXEL_FORMAT.GRAYSCALE; // Need Grayscale for Editor
		#else
		mPixelFormat = Image.PIXEL_FORMAT.RGB888; // Use RGB888 for mobile
		#endif

		// Register Vuforia life-cycle callbacks:
		VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
		VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
		VuforiaARController.Instance.RegisterOnPauseCallback(OnPause);

	}

	#endregion // MONOBEHAVIOUR_METHODS

	#region PRIVATE_METHODS

	void OnVuforiaStarted()
	{

		// Try register camera image format
		if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
		{
			Debug.Log("Successfully registered pixel format " + mPixelFormat.ToString());

			mFormatRegistered = true;
		}
		else
		{
			Debug.LogError(
				"\nFailed to register pixel format: " + mPixelFormat.ToString() +
				"\nThe format may be unsupported by your device." +
				"\nConsider using a different pixel format.\n");

			mFormatRegistered = false;
		}

	}

	/// <summary>
	/// Called each time the Vuforia state is updated
	/// </summary>
	void OnTrackablesUpdated()
	{
		if (mFormatRegistered)
		{
			if (mAccessCameraImage)
			{
				Vuforia.Image image = CameraDevice.Instance.GetCameraImage(mPixelFormat);

				if (image != null)
				{
//					Debug.Log(
//						"\nImage Format: " + image.PixelFormat +
//						"\nImage Size:   " + image.Width + "x" + image.Height +
//						"\nBuffer Size:  " + image.BufferWidth + "x" + image.BufferHeight +
//						"\nImage Stride: " + image.Stride + "\n"
//					);

					byte[] pixels = image.Pixels;

//					if (pixels != null && pixels.Length > 0)
//					{
//						Debug.Log(
//							"\nImage pixels: " +
//							pixels[0] + ", " +
//							pixels[1] + ", " +
//							pixels[2] + ", ...\n"
//						);
//					}
				}
			}
		}
	}

	public void TakeScreenshot ()
	{
		if (mFormatRegistered) {
			if (mAccessCameraImage) {
				CameraDevice cam = CameraDevice.Instance;
				Image image = cam.GetCameraImage (mPixelFormat);

				if (image == null) {
					Debug.Log (mPixelFormat + " image is not available yet");
				} else {
					string s = mPixelFormat + " image: \n";
					s += "  size: " + image.Width + "x" + image.Height + "\n";
					s += "  bufferSize: " + image.BufferWidth + "x" + image.BufferHeight + "\n";
					s += "  stride: " + image.Stride;

					Debug.Log (s);

					Texture2D tex = new Texture2D (image.Width, image.Height, TextureFormat.RGB24, false);
					image.CopyToTexture (tex);
					// tex.ReadPixels (new Rect(0, 0, image.Width, image.Height), 0, 0);
					tex.Apply ();

					byte[] bytes = tex.EncodeToPNG ();
					Destroy (tex);

					// For testing purposes, also write to a file in the project folder
					File.WriteAllBytes (Application.persistentDataPath + "/Screenshot.png", bytes);

					// now use Unity's built in
					ScreenCapture.CaptureScreenshot ("UnityScreenshot.png");

				}
			}
		}
	}

	/// <summary>
	/// Called when app is paused / resumed
	/// </summary>
	void OnPause(bool paused)
	{
		if (paused)
		{
			Debug.Log("App was paused");
			UnregisterFormat();
		}
		else
		{
			Debug.Log("App was resumed");
			RegisterFormat();
		}
	}

	/// <summary>
	/// Register the camera pixel format
	/// </summary>
	void RegisterFormat()
	{
		if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
		{
			Debug.Log("Successfully registered camera pixel format " + mPixelFormat.ToString());
			mFormatRegistered = true;
		}
		else
		{
			Debug.LogError("Failed to register camera pixel format " + mPixelFormat.ToString());
			mFormatRegistered = false;
		}
	}

	/// <summary>
	/// Unregister the camera pixel format (e.g. call this when app is paused)
	/// </summary>
	void UnregisterFormat()
	{
		Debug.Log("Unregistering camera pixel format " + mPixelFormat.ToString());
		CameraDevice.Instance.SetFrameFormat(mPixelFormat, false);
		mFormatRegistered = false;
	}

	#endregion //PRIVATE_METHODS
}